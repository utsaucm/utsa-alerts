<?php
$template_path = get_bloginfo('template_directory');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
  <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
  <title>Campus Alerts | The University of Texas at San Antonio</title>
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen, print" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <link rel="shortcut icon" href="<?php echo $template_path;?>/favicon.ico" />
	<?php // Get archive link page id
		$is_video = get_option('woo_vidpage'); // Name of the archives page
		$is_video_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '$is_video'");
		if ( is_page($is_video) or is_page($is_video_id) ) {
	?>
	<script type="text/javascript" src="//alerts.utsa.edu/wp-content/themes/alerts/includes/js/mootools.js"></script>
	<script type="text/javascript" src="//alerts.utsa.edu/wp-content/themes/alerts/includes/js/swfobject.js"></script>
  <?php include(TEMPLATEPATH . '/includes/js/analytics.js' ); ?>
  <?php } ?>
	<?php
		// Includes for WooThemes functions
		include(TEMPLATEPATH . '/includes/categories.php');
	?>
	<?php wp_head(); ?>
  <?php #include 'includes/js/analytics.js' ?>
</head>
<body>


	<div id="pages-top">
		<div class="container clearfix">
      <div id="topBar">
    	  <a href="http://utsa.edu/" title="The University of Texas at San Antonio Website">
          <img width="320" height="35" src="<?php echo $template_path;?>/images/logo4.png" alt="The University of Texas at San Antonio" border="0" style="padding-top:1.5rem;" />
        </a>
			</div>

			<div id="subscribe">
				<a class="rss" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); }
				else { echo get_bloginfo_rss('rss2_url'); } ?>" title="<?php _e('Subscribe to Alerts RSS Feed',woothemes); ?>">Subscribe to Alerts <i class="icon-rss fa-alerts-rss" aria-hidden="true"></i></a>
			</div>
      <div style="clear:Both;"></div>
		</div>
	</div>

	<div id="header">
		<div class="container clearfix">
			<div>
				<h1><?php bloginfo('name'); ?></h1>
				<div id="search">
					<?php include(TEMPLATEPATH . '/searchform.php'); ?>
				</div>
			</div>
		</div>
	</div>

	<div id="categories">
		<div class="container">
	      <ul class="category-list clearfix" style="float:left;">
          <li<?php if(is_home()): ?> class="current-cat"<?php endif; ?>>
            <a href="<?php bloginfo('url'); ?>"><?php _e('Home',woothemes); ?></a>
          </li>
          <?php wp_list_categories('exclude=1,7,22,23&title_li=');
          ?>
          <?php //wp_list_categories('exclude=1,7,3,6&title_li=');
          ?>
          <li class="cat-item"><a style="color:#f47321; cursor:pointer;" href="/utsa-community-emergency-response/">Emergency Response Guide</a></li>
          <li class="cat-item"><a style="color:#f47321; cursor:pointer;">Preparedness</a>
            <ul class="children">
            	<?php wp_list_pages('exclude=226,60&title_li='); ?>
              <li><a href="http://www.utsa.edu/Safety/#/safetymanuals">Safety Manuals</a></li>
            </ul>
          </li>
          <li class="cat-item"><a style="color:#f47321; cursor:pointer;">Supporting Offices</a>
          	<ul class="children">
              <li><a href="http://utsa.edu/utsapd/bcem/about_bcem/">Business Continuity &amp; Emergency Management</a></li>
              <li><a href="http://utsa.edu/utsapd/">UTSA Police Department</a></li>
              <li><a href="http://www.utsa.edu/Safety/">Environmental Health, Safety, &amp; Risk Management</a></li>
              <li><a href="http://www.utsa.edu/health/">Student Health Services</a></li>
              <li><a href="http://www.utsa.edu/counsel/">Student Counseling Services</a></li>
              <li><a href="http://www.utsa.edu/ucomm/">University Communications</a></li>
              <li><a href="http://www.utsa.edu/oit/">Information Technology</a></li>
            </ul>
          </li>
          <li class="alerts-social" ><a href="http://twitter.com/UTSA_Police" style=""><i class="icon-twitter fa-alerts-color" aria-hidden="true"></i></a></li>
          <li class="alerts-social" ><a href="http://www.facebook.com/pages/UTSA_Police_Dept/184498001573459" style=""><i class="icon-facebook-squared fa-alerts-color" aria-hidden="true"></i>
          </a></li>
        </ul>
    		<div style="clear:both;"></div>
    	</div>
	</div>
