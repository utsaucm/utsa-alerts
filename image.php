<?php get_header(); ?>
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); $preview = get_post_meta($post->ID, 'preview', true); ?>
		<div id="featured">
			<div class="container">
				<div class="featured-small clearfix">
					<h2 class="featured"><?php echo get_the_title($post->post_parent); ?></a> &raquo; <?php the_title(); ?></h2>
				</div>
			</div>
		</div>
		<div id="content">

		<div class="container clearfix">
			<div id="left-col">
				<ul class="post-list clearfix">
					<li class="post clearfix">
						<div class="meta">
							<h3><?php the_category(', ') ?></h3>
								<p><?php _e('Posted on',woothemes); ?> <?php the_time('F jS, Y') ?></p>
								<p><?php _e('Written by',woothemes); ?> <?php the_author(); ?></p>
					
							<h4 class="related-posts"><?php _e('Related Posts',woothemes); ?></h4>
								<ul class="related_posts">
										<?php wp_related_posts(); ?>
									</ul>
								
							<h4 class="tags"><?php _e('Tags',woothemes); ?></h4>
								<?php the_tags( '', ', ', ''); ?>
						</div>
						<div class="post-content">
							<p class="attachment"><a href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'medium' ); ?></a></p>
							<div class="box">
								<span class="caption"><?php if ( !empty($post->post_excerpt) ) the_excerpt(); // this is the "caption" ?></span>
								<?php the_content(__('<p class="serif">Read the rest of this entry &raquo;</p>',woothemes)); ?>
							</div>
								<br /><br />
							<div class="navigation clearfix">
								<div class="left img-border"><?php previous_image_link() ?></div>
								<div class="right img-border"><?php next_image_link() ?></div>
							</div>
								<br /><br />
							<div class="box small arial">
								<?php _e('This entry was posted on',woothemes); ?> <?php the_time('l, F jS, Y') ?> <?php _e('at',woothemes); ?> <?php the_time() ?>
								<?php _e('and is filed under',woothemes); ?> <?php the_category(', ') ?>.
								<?php _e('You can follow any responses to this entry through the',woothemes); ?> <?php post_comments_feed_link('RSS 2.0'); ?> <?php _e('feed',woothemes); ?>.
		
								<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
									// Both Comments and Pings are open ?>
									<?php _e('You can',woothemes); ?> <a href="#respond"><?php _e('leave a response',woothemes); ?></a>, <?php _e('or',woothemes); ?> <a href="<?php trackback_url(); ?>" rel="trackback"><?php _e('trackback',woothemes); ?></a> <?php _e('from your own site',woothemes); ?>.
		
								<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
									// Only Pings are Open ?>
									<?php _e('Responses are currently closed, but you can',woothemes); ?> <a href="<?php trackback_url(); ?> " rel="trackback"><?php _e('trackback',woothemes); ?></a> <?php _e('from your own site',woothemes); ?>.
		
								<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
									// Comments are open, Pings are not ?>
									<?php _e('You can skip to the end and leave a response. Pinging is currently not allowed.',woothemes); ?>
		
								<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
									// Neither Comments, nor Pings are open ?>
									<?php _e('Both comments and pings are currently closed.',woothemes); ?>
		
								<?php } edit_post_link(__('Edit this entry.',woothemes),'',''); ?>
							</div>
						</div>
					</li>
						<?php comments_template(); ?>
				</ul>
			</div>
			<div id="right-col">
				<?php get_sidebar(); ?>
			</div>
		</div>
		<?php endwhile; ?>
	<?php else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.',woothemes); ?></p>
	<?php endif; ?>
		</div>
<?php get_footer(); ?>
