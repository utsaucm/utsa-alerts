
<?php if(is_home()) : ?>
<div id="featured">
	<div class="container">
		<div class="featured-blog clearfix">
			 <div class="featured-blog-content">
				<h2 class="featured"><?php _e('About the Blog',woothemes); ?></h2>
				<p><?php echo stripslashes(get_option('woo_bio')); ?> <?php if (get_option('woo_about')) { ?> <a href="<?php echo get_option('woo_about'); ?>" title="<?php _e('Read more about me',woothemes); ?>"><?php _e('Read more...',woothemes); ?></a> <?php } ?></p>
			</div>
			<div class="featured-links">
				<?php if ( get_option('woo_twitter') ) { ?>
				<a href="http://www.twitter.com/<?php echo get_option('woo_twitter'); ?>" class="twitter">
					<span class="medium block">Twitter</span>
					<span class="white extrasmall verdana"><?php _e('Stay updated on',woothemes); ?> Twitter</span>
				</a>
				<?php } ?>
				<?php if ( get_option('woo_email') ) { ?>                
				<a href="mailto:<?php echo get_option('woo_email'); ?>" class="email">
					<span class="medium block"><?php _e('Email Me',woothemes); ?></span>
					<span class="white extrasmall verdana"><?php _e('Send me a Message',woothemes); ?></span>
				</a>
				<?php } ?>                
				<a href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>" class="rss-big">
					<span class="medium block"><?php _e('RSS Feed',woothemes); ?></span>
					<span class="white extrasmall verdana"><?php _e('Subscribe to Updates',woothemes); ?></span>
				</a>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>