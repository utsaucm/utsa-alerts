window.addEvent('domready', function() {
	/* var accordion = new Accordion($$('.toggler'),$$('.element'), { pre-MooTools More */
	var accordion = new Fx.Accordion($$('.toggler'),$$('.element'), {
		opacity: 0,
		onActive: function(toggler) { toggler.setStyle('background', '#002a5c'); toggler.setStyle('color', '#fff'); },
		onBackground: function(toggler) { toggler.setStyle('background', '#eee'); toggler.setStyle('color', '#000'); }
	});
});