<?php get_header(); ?>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); $preview = get_post_meta($post->ID, 'preview', true); ?>


		<div id="content">

            <div class="container clearfix">
                <div id="left-col">
                    <ul class="post-list clearfix">
                        
                        	<h2 class="home-subhead" style="margin-bottom:0px; padding-bottom:0px;"></h2>
                            
                            <div class="post-content">
                             	<h1 style="color:#f47321;"><?php the_title(); ?></h1>   
								
								<?php if ( get_option('woo_image_disable') == 'false' ) { ?> 
        
                                <?php woo_get_image('image',get_option('woo_image_width'),get_option('woo_image_height'),'thumb alignleft'); ?>
        
                                <?php } ?>
                                
                                <?php the_content(__('Continue Reading...',woothemes)); ?>
                                
                            </div>
                        
                        
                        	<div class="box small arial" style="border-top:1px solid #aaa; margin:0px 20px; padding:10px 0px; color:#999; line-height:17px;">
                                This entry was posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?> by <?php the_author_posts_link(); ?>. In case of emergency, please review the <a href="/utsa-community-emergency-response/">Community Emergency Response Guide</a>.
                                <?php edit_post_link('Edit this entry.','',''); ?>
                            </div>

                        
                        
                        </li>
                                                    
							<!--<?php# comments_template(); ?>-->
                    </ul>
                </div>
                <div id="right-col">
                    <?php get_sidebar(); ?>
                </div>
            </div>
            <?php endwhile; ?>
        <?php else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.',woothemes); ?></p>
        <?php endif; ?>
        </div> <!-- / content -->
<?php get_footer(); ?>
