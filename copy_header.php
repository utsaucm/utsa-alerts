<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

    <title>Campus Alerts | UTSA | The University of Texas at San Antonio</title>

	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen, print" />
    
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	
	<?php // Get archive link page id
		$is_video = get_option('woo_vidpage'); // Name of the archives page
		$is_video_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '$is_video'");							
		if ( is_page($is_video) or is_page($is_video_id) ) { 
	?>

	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/js/mootools.js"></script>	
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/js/swfobject.js"></script>	
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/videobox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/js/videobox.js"></script>				
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/js/moo-accordian.js"></script>				

	<?php } ?>

    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/js/suckerfish.js"></script>	

    <!--[if lt IE 7]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE7.js" type="text/javascript"></script>
    <![endif]-->

	<?php 
		// Includes for WooThemes functions
		include(TEMPLATEPATH . '/includes/categories.php');
		include(TEMPLATEPATH . '/includes/stylesheet.php');
	?>	
		
	<?php wp_head(); ?>
<!-- Google Analytics code includes -->
<script type="text/javascript"> 
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2438226-1']);
  _gaq.push(['_setDomainName', '.utsa.edu']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script> 
<!-- end Google Analytics code includes -->

</head>

<body>

	<?php $template_path = get_bloginfo('template_directory'); $GLOBALS['defaultgravatar'] = $template_path . '/images/gravatar.jpg'; ?>

	<div id="pages-top">
		<div class="container clearfix">
               <!--<li class="blank"><a href="<?php# bloginfo('url'); ?>" title="<?php# _e('Return Home',woothemes); ?>"><?php# _e('Home',woothemes); ?></a></li>
				<?php# wp_list_pages('sort_column=menu_order&title_li=&depth=3'); ?>-->
                
            <div id="topBar">
            	<a href="http://utsa.edu/" title="The University of Texas at San Antonio Website"><img src="/img/utsaMark-20x60.png" alt="UTSA" border="0" />
                The University of Texas at San Antonio</a>
			</div>
            
			<div id="subscribe">
				<a class="rss" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } 
				else { echo get_bloginfo_rss('rss2_url'); } ?>" title="<?php _e('Subscribe to Alerts RSS Feed',woothemes); ?>"><?php _e('Subscribe to Alerts',woothemes); ?></a>
			</div>
		</div>
	</div>
    
	<div id="header">
		<div class="container clearfix">
			<div id="logo-back">
				<h1><?php bloginfo('name'); ?></h1>
				<a href="<?php bloginfo('url'); ?>"><img src="<?php if ( get_option('woo_logo') <> "" ) { echo get_option('woo_logo'); } else { ?><?php bloginfo('template_directory'); ?>/styles/<?php echo $style_path; ?>/logo-trans.png<?php } ?>" alt="<?php bloginfo('name'); ?>" /></a>
				<div id="search">
					<?php include(TEMPLATEPATH . '/searchform.php'); ?>
				</div>
			</div>
		</div>
	</div>
    
	<div id="categories">
		<div class="container">
	      <ul class="category-list clearfix" style="float:left;">
            
            <li<?php if(is_home()): ?> class="current-cat"<?php endif; ?>>
            <a href="<?php bloginfo('url'); ?>"><?php _e('Home',woothemes); ?></a></li>
	            <?php wp_list_categories('exclude=1,7,22,23&title_li='); ?>
                
            <li class="cat-item"><a style="color:#f47321; cursor:pointer;" href="/utsa-community-emergency-response/">Emergency Response Guide</a>
            
            <li class="cat-item"><a style="color:#f47321; cursor:pointer;">Preparedness</a>
            <ul class="children">
            	<?php wp_list_pages('exclude=226,60&title_li='); ?>
                <li><a href="http://www.utsa.edu/Safety/#/safetymanuals">Safety Manuals</a></li>
            </ul>
            </li>
            
            <li class="cat-item"><a style="color:#f47321; cursor:pointer;">Supporting Offices</a>
            	<ul class="children">
                <li><a href="http://utsa.edu/utsapd/bcem/about_bcem/">Business Continuity &amp; Emergency Management</a></li>
                <li><a href="http://utsa.edu/utsapd/">UTSA Police Department</a></li>
                <li><a href="http://www.utsa.edu/Safety/">Environmental Health, Safety, &amp; Risk Management</a></li>
                <li><a href="http://www.utsa.edu/health/">Student Health Services</a></li>
                <li><a href="http://www.utsa.edu/counsel/">Student Counseling Services</a></li>
                <li><a href="http://www.utsa.edu/ucomm/">University Communications</a></li>
                <li><a href="http://www.utsa.edu/oit/">Information Technology</a></li>
                </ul>
            </li>
          </ul>
    
          <!--<ul class="category-list-rt clearfix" style="float:right;">
          	<li class="cat-item"><a href="#">Preparedness</a>
            <ul class="children">
            	<?php# wp_list_pages('title_li='); ?>
            </ul>
            </li>
          </ul>-->
	
    		<div style="clear:both;"></div>
    	</div>
	</div>
	
