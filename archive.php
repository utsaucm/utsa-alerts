<?php get_header(); ?>
	<?php if (have_posts()) : ?>

        
		<div id="content">

            <div class="container clearfix">
                <div id="left-col">
                
                	<?php $post = $posts[0];  ?>
					<?php if (is_category()) { ?>
						<h2 class="home-subhead"><?php _e('Alerts related to',woothemes); ?> "<?php single_cat_title(); ?>"</h2>
					<?php } elseif( is_tag() ) { ?>
						<h2 class="home-subhead"><?php _e('Posts Tagged with',woothemes); ?> "<?php single_tag_title(); ?>"</h2>
					<?php } elseif (is_day()) { ?>
						<h2 class="home-subhead"><?php _e('Alerts related to',woothemes); ?> "<?php the_time('F jS, Y'); ?>"</h2>
					<?php } elseif (is_month()) { ?>
						<h2 class="home-subhead"><?php _e('Alerts related to',woothemes); ?> "<?php the_time('F, Y'); ?>"</h2>
					<?php } elseif (is_year()) { ?>
						<h2 class="home-subhead"><?php _e('Alerts related to',woothemes); ?> "<?php the_time('Y'); ?>"</h2>
					<?php } elseif (is_author()) { ?>
						<h2 class="home-subhead"><?php _e('"Author" Archive',woothemes); ?></h2>
					<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
						<h2 class="home-subhead"><?php _e('"Blog" Archives',woothemes); ?></h2>
					<?php } ?>
                    
                
                    <ul class="post-list clearfix">
                    <?php while (have_posts()) : the_post(); $preview = get_post_meta($post->ID, 'preview', true); ?>
                    <li class="post clearfix">
                    
                    <div class="post-content">
                    
                    <p class="meta2"><?php _e('Posted on',woothemes); ?> <?php the_time('F j, Y') ?></p>
                    <h2 class="title2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to',woothemes); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    
                    
                    </div>
                    </li>
                    
                    
                    <?php endwhile;?>
                    </ul>
                    
       
                    
                </div>
                <div id="right-col">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.',woothemes); ?></p>
        <?php endif; ?>
		</div>
<?php get_footer(); ?>