<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.',woothemes); ?></p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->

<li class="post-blank clearfix">
	<h1 class="comments-title"><?php comments_number(__('0 Comments',woothemes), __('1 Comment',woothemes), __('% Comments',woothemes) );?></h1>
	<h2 class="comments-title"><?php _e('Take a look at some of the responses we have had to this article.',woothemes); ?></h2>
</li>
<li class="post clearfix">
	<?php if ( have_comments() ) : ?>
    
    <div id="comments_wrap">   
        <ol class="commentlist">
        <?php wp_list_comments('avatar_size=48&callback=custom_comment'); ?>
        </ol>
        <div class="navigation">
            <div class="fl"><?php previous_comments_link() ?></div>
            <div class="fr"><?php next_comments_link() ?></div>
            <div class="fix"></div>
        </div>
    </div> <!-- end #comments_wrap -->
     
	<?php else : // this is displayed if there are no comments so far ?>
		<?php if ('open' == $post->comment_status) : ?>
			<!-- If comments are open, but there are no comments. -->
		<?php else : // comments are closed ?>
			<!-- If comments are closed. -->
			<p class="nocomments"><?php _e('Comments are closed.',woothemes); ?></p>
		<?php endif; ?>
	<?php endif; ?>
</li>

<li class="post-blank clearfix">
	<h1 class="comments-title"><?php comment_form_title( __('Leave a Reply',woothemes), __('Leave a Reply to %s',woothemes) ); ?></h1>
	<h2 class="comments-title"><?php _e('Let us know what you thought.',woothemes); ?> <?php cancel_comment_reply_link(); ?></h2>

</li>
<li class="post-last-blank clearfix">
<div id="respond">
	<?php if ('open' == $post->comment_status) : ?>
	
	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p><?php _e('You must be',woothemes); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in',woothemes); ?></a> <?php _e('to post a comment.',woothemes); ?></p>
	<?php else : ?>

	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform" class="post-content clearfix">
		<div class="comment-form-left">

			<?php if ( $user_ID ) : ?>
				<p><?php _e('Logged in as',woothemes); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(); ?>" title="<?php _e('Log out of this account',woothemes); ?>"><?php _e('Log out',woothemes); ?> &raquo;</a></p>
			<?php else : ?>
				<p><span class="small block"><?php _e('Name',woothemes); ?><?php if ($req) _e('(required)',woothemes) ?>:</span>
					<input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="30" tabindex="1" class="textfield" />
				</p>
				
				<p><span class="small block"><?php _e('Email',woothemes); ?><?php if ($req) _e('(required)',woothemes) ?>:</span>
					<input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="30" tabindex="2"  class="textfield" />
				</p>
				
				<p><span class="small block"><?php _e('Website',woothemes); ?>:</span>
					<input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="30" tabindex="3" class="textfield" />
				</p>
			<?php endif; ?>
				
		</div>
		<div class="comment-form-right">
			<p><?php _e('Message',woothemes); ?>:
				<textarea name="comment" id="comment" tabindex="4"  rows="10" cols="45" class="textfield"></textarea>
			</p>
            <p><input name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Submit your comment',woothemes); ?>" class="button" /></p>
			<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
			<?php comment_id_fields(); ?>
            <?php do_action('comment_form', $post->ID); ?>
		</div>	
	</form>
	<?php endif; // If registration required and not logged in ?>
</div> <!-- end #respond -->
</li>
<?php endif; // if you delete this the sky will fall on your head ?>