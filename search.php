<?php get_header(); ?>
        
		<div id="content">

		<div class="container clearfix">
			<div id="left-col">
				<ul class="post-list-last clearfix">
                
                <h2 class="home-subhead"><?php _e('Search',woothemes); ?> <?php _e('Results',woothemes); ?></h2>
                
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); $preview = get_post_meta($post->ID, 'preview', true); ?>
						<li class="post clearfix">
						
                        
                        <div class="post-content">
                    
                    <p class="meta2"><?php _e('Posted on',woothemes); ?> <?php the_time('F j, Y') ?></p>
                    <h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to',woothemes); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    
                    
                    </div>
                    </li>
                    
                    
						<?php endwhile; ?>
                        
                      
					<?php else: ?>
						<h2 class="center"><?php _e('No posts found. Try a different search?',woothemes); ?></h2>
						<?php include (TEMPLATEPATH . '/searchform.php'); ?>
					<?php endif; ?>
				</ul>
			</div>
			<div id="right-col">
				<?php get_sidebar(); ?>
			</div>
		</div>
<?php get_footer(); ?>