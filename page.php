<?php get_header(); ?>
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); $preview = get_post_meta($post->ID, 'preview', true); ?>

        <div id="content">
            <div class="container clearfix">
                <div id="left-col" style="border-top:3px solid #eee;">
                    <ul class="post-list clearfix">
                        <li class="post-last clearfix">

                        <h2 class="home-subhead" style="margin-bottom:0px; padding-bottom:0px;"></h2>

                            <div class="post-content">

                                <h1 style="color:#f47321;"><?php the_title(); ?></h1>
								<?php the_content(__('Continue Reading...',woothemes)); ?>
                            </div>
                        </li>
                    </ul>
                </div>
                <div id="right-col">
                    <?php get_sidebar(); ?>
                </div>
            </div>
            <?php endwhile; ?>
            <?php else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.',woothemes); ?></p>
            <?php endif; ?>
        </div>

<?php get_footer(); ?>
