<div id="footer">
   <div class="container clearfix" style="padding:13px 0px 30px 0px;">
      <div style="float:left;">
       	<p style="color:#002a5c; font-size:11px; line-height:18px;">
          Campus Alerts is the UTSA emergency website developed to keep students, faculty, staff and the general public informed during emergency situations.
          <br />
          This website is maintained by the
          <a href="http://www.utsa.edu/utsapd/bcem/about_bcem/" style="margin-right:0; padding-right:0;">
            Office of Business Continuity and Emergency Management
          </a> and by
          <a href="http://www.utsa.edu/ucomm">University Communications.</a>
        </p>
        <p class="copyright">Copyright &copy; 2017 <?php #bloginfo('name'); ?> | The University of Texas at San Antonio, One UTSA Circle, San Antonio, TX 78249-1644 | Information: 210-458-4011 | UTSA Police: (210) 458-4011</p>
        <a href="http://www.utsa.edu/directory/contact/">Contact</a> | <a href="http://www.utsa.edu/identity/">Identity Guidelines</a> | <a href="http://www.utsa.edu/policies.html">Policies</a> | <a href="http://www.utsa.edu/utsapd/bcem/about_bcem/">Emergency Preparedness</a> |
        <a href="http://www.utsa.edu/reqlinks.html">Required Links</a> | <a href="http://www.utsa.edu/openrecords/">Open Records</a> | <a href="http://www.utsa.edu/acrs/compliance/comply_hotline.htm">Report Fraud</a>
      </div>
      <div style="clear:both;"></div>
    </div>
  </div>
  <?php wp_footer(); ?>
</div>
</body>
</html>
