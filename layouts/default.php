<div id="content">    
    <div class="container clearfix">
        <div id="left-col">      
        
        <h2 class="home-subhead">Current Major Alerts</h2>
        <?php query_posts('cat=23&showposts=19'); ?>
        <ul class="post-list clearfix">
		<?php while (have_posts()) : the_post(); ?>
            <li class="post clearfix">
            
            <div class="post-content">
                
                <p class="meta2"><?php _e('Posted on',woothemes); ?> <?php the_time('F j, Y') ?><!--, <?php # _e('by:',woothemes); ?> <?php #the_author_posts_link(); ?>--></p>
                <h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to',woothemes); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            
                <!-- Custom setting image -->
                <?php # woo_get_image('image',get_option('woo_thumb_width'),get_option('woo_thumb_height'),'thumb alignleft'); ?> 	
                
                <?php # if ( get_option('woo_the_content') == 'true' ) { the_content('Continue Reading...'); } else { the_excerpt(); } ?>
                
                <?php the_content(); ?>
            </div>
            </li>
        
        
		<?php endwhile;?>
		</ul>
        
        <h2 class="home-subhead">Recent Announcements</h2>
        <?php query_posts('cat=-1&showposts=7'); ?>
        <ul class="post-list clearfix">
		<?php while (have_posts()) : the_post(); ?>
            <li class="post clearfix">
            
            <div class="post-content">
                
                <p class="meta2"><?php _e('Posted on',woothemes); ?> <?php the_time('F j, Y') ?></p>
                <h2 class="title2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to',woothemes); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                
            </div>
            </li>
        
        
		<?php endwhile;?>
		</ul>
        
        
        
        
        </div>
        
        <div id="right-col">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<!-- /content -->        